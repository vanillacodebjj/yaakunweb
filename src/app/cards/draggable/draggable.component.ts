import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Subject } from 'rxjs';
import swal from "sweetalert2";

@Component({
  selector: 'app-draggable',
  templateUrl: './draggable.component.html',
  styleUrls: ['./draggable.component.scss']
})
export class DraggableComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject <any> = new Subject();
  id_categoria: any;
  conv: any = [];

  constructor( public http: HttpClient) {
    this.id_categoria = localStorage.getItem('id_categoria');
    this.convocatorias();
  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json'
      },
    };
  }

  convocatorias() {
    this.http.post('http://www.yoloiztac.com.mx/yolo/yaakun/getConvocatoriaC.php', {id_categoria: this.id_categoria}).subscribe((data: any) => {
      console.log(data);
      if (data === null) {
        swal.fire({
          title: '¡Lo sentimos!',
          text: 'Por ahora no contamos con servicios en esta categoria',
          imageUrl: 'assets/images/sad.svg',
          imageWidth: 400,
          imageHeight: 200,
          imageAlt: 'Custom image'
        });
        this.dtTrigger.next();
      } else {
        this.conv = data;
        this.dtTrigger.next();
      }
    });
  }
}
