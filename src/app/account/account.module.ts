import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AccountRoutes } from './account.routing';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { ForgotComponent } from './forgot/forgot.component';
import { InstitucionesComponent } from './instituciones/instituciones.component';
import { ConocenosComponent } from './conocenos/conocenos.component';
import { PatrocinadoresComponent } from './patrocinadores/patrocinadores.component';
import { InicioComponent } from './inicio/inicio.component';
import { LockscreenComponent } from './lockscreen/lockscreen.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AccountRoutes),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    SigninComponent,
    SignupComponent,
    ForgotComponent,
    LockscreenComponent,
    InicioComponent,
    InstitucionesComponent,
    ConocenosComponent,
    PatrocinadoresComponent
  ]
})
export class AccountModule {}
