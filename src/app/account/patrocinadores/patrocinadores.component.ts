import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import {AngularFireAuth} from '@angular/fire/auth';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-patrocinadores',
  templateUrl: './patrocinadores.component.html',
  styleUrls: ['./patrocinadores.component.scss']
})
export class PatrocinadoresComponent implements OnInit {
  public form: FormGroup;
  id_municipio: any;
  estados: any[];
  municipios: any[];
  patrocinadores: any[];
  constructor(private fb: FormBuilder, private router: Router, public auth: AngularFireAuth, public http: HttpClient) {
    this.getPatrocinadores();
    this.getEstados();
  }

  ngOnInit() {
    this.form = this.fb.group({
      uname: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])]
    });
  }

  onSubmit() {
     console.log('usuario : ' );
    // this.router.navigate(['/']);
  }
  login(username, pass) {
    console.log('usuario : ' + username);
    console.log('pass : ' + pass);

    if (!username || !pass) {
      alert('Campos vacios, favor de llenarlos');
    } else {
      this.auth.auth.signInWithEmailAndPassword(username, pass).then(data => {
        console.log(data);
        localStorage.setItem('username', username);
        this.router.navigate(['/']);
        /*this.http.post('https://150.40.9.7/webService/login.php',
          {correo: email}).subscribe((data2: any) => {
          console.log(data2[0].nombre);
          console.log(data2[0].id_usuario);
          if (data2.respuesta === 'Vacio') {
            alert('Ocurrio un error, favor de inetentarlo más tarde');
          } else {
            localStorage.setItem('nombreU', data2[0].nombre);
            localStorage.setItem('id_usuario', data2[0].id_usuario);
            this.router.navigate(['/dashboard']);
          }
        });*/
      }, error => {
        console.log(error);
        console.log(error.code);
        if (error.code === 'auth/wrong-password') {
          alert('La contraseña es incorrecta, favor de verificarla');
        } else {
          alert('El usuario no existe, favor de verificarlo');
        }

      });
    }
  }

  getEstados() {
    this.http.get('http://www.yoloiztac.com.mx/yolo/yaakun/getEstados.php').subscribe((data: any) => {
      console.log('est: ' + data);
      this.estados = data;
    });
  }

  getMunicipios() {
    this.http.post('http://www.yoloiztac.com.mx/yolo/yaakun/getMunicipios.php', {
        id: this.id_municipio,
        opcion: 'getMunicipioEstado'
      }
    ).subscribe(
      (data: any) => {
        console.log('mun: ' + data);
        this.municipios = data;
        // this.verificarEditar(this.respuesta);
        // this.respuesta = this.resultado[0].respuesta;
      }
    );
  }

  getPatrocinadores() {
    this.http.get('http://www.yoloiztac.com.mx/yolo/yaakun/getPatrocinadores.php').subscribe((data: any) => {
      console.log(data);
      this.patrocinadores = data;
    });
  }

  registrar() {}

}
