import { Routes } from '@angular/router';

import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { ForgotComponent } from './forgot/forgot.component';
import { LockscreenComponent } from './lockscreen/lockscreen.component';
import { InicioComponent } from './inicio/inicio.component';
import { InstitucionesComponent } from './instituciones/instituciones.component';
import { ConocenosComponent } from './conocenos/conocenos.component';
import { PatrocinadoresComponent } from './patrocinadores/patrocinadores.component';

export const AccountRoutes: Routes = [
  {
    path: '',
    children: [
      /*{
        path: '',
        component: InicioComponent
      },*/
      {
        path: 'signin',
        component: SigninComponent
      },
      {
        path: 'signup',
        component: SignupComponent
      },
      {
        path: 'forgot',
        component: ForgotComponent
      },
      {
        path: 'lockscreen',
        component: LockscreenComponent
      },
      {
        path: 'inicio',
        component: InicioComponent
      },
      {
        path: 'instituciones',
        component: InstitucionesComponent
      }
      ,
      {
        path: 'conocenos',
        component: ConocenosComponent
      }
      ,
      {
        path: 'patrocinadores',
        component: PatrocinadoresComponent
      }
    ]
  }
];
