import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import {AngularFireAuth} from '@angular/fire/auth';
import {HttpClient} from '@angular/common/http';
//  SweetAlert ♥
import swal from'sweetalert2';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  public form: FormGroup;

  constructor(private fb: FormBuilder, private router: Router, public auth: AngularFireAuth, public http: HttpClient) {
    /// localStorage.clear();
  }

  ngOnInit() {
    this.form = this.fb.group({
      uname: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])]
    });
  }

  onLoggedin() {
    localStorage.setItem('isLoggedin', 'true');
  }

  onSubmit() {
     console.log('usuario : ' );
    // this.router.navigate(['/']);
  }
  login(username, pass) {
    console.log('usuario : ' + username);
    console.log('pass : ' + pass);

    if (!username || !pass) {
      swal.fire({  type: 'warning',
        title: '¡Campos vacios!',
        text: 'Favor de llenar los campos',
        confirmButtonColor: '#400342',
        customClass: {
          popup: 'animated tada'
        }
      });
    } else {
      this.auth.auth.signInWithEmailAndPassword(username, pass).then(data => {
        console.log(data);
        this.http.post('http://www.yoloiztac.com.mx/yolo/yaakun/login.php',
          {email: username}).subscribe((data2: any) => {
          if (data2 === null) {
            swal.fire({  type: 'error',
              title: 'Oops...',
              text: 'Ocurrio un error, favor de intentarlo más tarde',
              confirmButtonColor: '#400342',
              customClass: {
                popup: 'animated tada'
              }
            });
          } else {
            localStorage.setItem('username', username);
            localStorage.setItem('id_usuario', data2[0].id_usuario);
            this.router.navigate(['/']);
          }
        });
      }, error => {
        console.log(error);
        console.log(error.code);
        if (error.code === 'auth/wrong-password') {
          // alert('La contraseña es incorrecta, favor de verificarla');
          swal.fire({  type: 'error',
            title: 'Oops...',
            text: 'La contraseña es incorrecta, favor de verificarla',
            confirmButtonColor: '#400342',
            customClass: {
              popup: 'animated tada'
            }
          });
        } else {
          // alert('El usuario no existe, favor de verificarlo');

          swal.fire({  type: 'error',
            title: 'Oops...',
            text: 'El usuario no existe, favor de verificarlo',
            confirmButtonColor: '#400342',
            customClass: {
              popup: 'animated tada'
            }
          });
        }

      });
    }
  }

}
