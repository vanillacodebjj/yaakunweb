import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {HttpClient} from '@angular/common/http';
@Component({
  selector: 'app-categorias-vtas',
  templateUrl: './categorias-vtas.component.html',
  styleUrls: ['./categorias-vtas.component.scss']
})
export class CategoriasVtasComponent implements OnInit {
  admin: any;
  usuario: any;
  rol: any;
  agregar: any;
  descripcion: any;
  descripcion_a: any;
  editarCat: any;
  lista: any;
  id_categoria_ing_gas: any;
  id_cat: any;
  mensaje: any;
  nombre: any;
  nombre_a: any;
  respuesta: any;
  user: any;
  categorias: any [];
  resultado: any [];
  constructor(private http: HttpClient, public router: Router) {
    this.lista = true;
    this.editarCat = false;
    this.agregar = false;
    this.getCategorias();
    this.rol = localStorage.getItem('rol');
    switch (this.rol) {
      case 'Admin':
        this.admin = true;
        this.usuario = true;
        break;
      case 'Usuario':
        this.admin = false;
        this.usuario = true;
        break;
    }
  }

  ngOnInit() {
  }

  agregarCategoria() {
    this.nombre = '';
    this.descripcion = '';
    this.agregar = true;
    this.lista = false;
    this.editarCat = false;
  }

  backCat() {
    this.lista = true;
    this.editarCat = false;
    this.agregar = false;
  }

  editarCategoria(id) {
    this.id_cat = id;
    this.editarCat = true;
    this.lista = false;
    this.agregar = false;
    this.getInfoCat();
  }

  eliminarCat(id) {
    this.http.post('http://localhost:8080/srv-3s/crudCategorias.php', {
        id: id,
        opcion: 'eliminarCat'
      }
    ).subscribe(
      (data: any) => {
        this.resultado = data;
        this.respuesta = data.respuesta;
        console.log('res: ' + this.respuesta);
        this.router.navigate(['/blank-page']);
      }
    );
  }

  eliminarCatConfirmar(id) {
    this.mensaje = confirm('Al aceptar se borrará la categoria y los productos que le pertenecen');
    if (this.mensaje) {
      this.eliminarCat(id);
    } else {
      alert('¡No se elimino!');
    }
  }

  eliminarPreCat(id, nombre) {
    this.mensaje = confirm('¿Desea remover la categoria ' + nombre + ' ?');
    if (this.mensaje) {
      this.eliminarCatConfirmar(id);
    } else {
      // alert('¡No se elimino!');
    }
  }

  getCategorias() {
    this.http.get('http://localhost:8080/srv-3s/getCategoriasIngGas.php').subscribe(
      (data: any) => {
        // console.log(data);
        this.categorias = data;
        // this.id = this.categorias[0];
      }
    );
  }
  getInfoCat() {
    this.http.post('http://localhost:8080/srv-3s/crudCategorias.php', {
        id: this.id_cat,
        opcion: 'getInfoCat'
      }
    ).subscribe(
      (data: any) => {
        this.resultado = data;
        this.nombre = this.resultado[0].nombre;
        this.descripcion = this.resultado[0].descripcion;
        // this.respuesta = this.resultado[0].respuesta;
      }
    );
  }

  guardarCategoria() {
    /*this.nombre = ((document.getElementById('nombre_a') as HTMLInputElement).value);
    this.descripcion = ((document.getElementById('descripcion_a') as HTMLInputElement).value);*/
    if (this.nombre_a === '' || !this.nombre_a) {
      alert('El campo nombre no puede ir vacio');
    } else {
      this.http.post('http://localhost:8080/srv-3s/crudCategorias.php', {
          id: this.id_cat,
          nombre: this.nombre_a,
          descripcion: this.descripcion_a,
          opcion: 'registrarCategoria'
        }
      ).subscribe(
        (data: any) => {
          this.resultado = data;
          this.respuesta = data.respuesta;
          console.log('res: ' + this.respuesta);
          this.verificarCrear(this.respuesta);
          this.editarCat = false;
          this.lista = true;
          // this.respuesta = this.resultado[0].respuesta;
        }
      );
      this.router.navigate(['/blank-page']);
    }
  }

  guardarCategoriaEditar() {
   if (!this.nombre) {
     alert('El campo nombre no puede ir vacio');
   } else {
     this.http.post('http://localhost:8080/srv-3s/crudCategorias.php', {
         id: this.id_cat,
         nombre: this.nombre,
         descripcion: this.descripcion,
         opcion: 'editarCategoria'
       }
     ).subscribe(
       (data: any) => {
         this.resultado = data;
         this.respuesta = data.respuesta;
         console.log('res: ' + this.respuesta);
         this.verificarEditar(this.respuesta);
         // this.respuesta = this.resultado[0].respuesta;
       }
     );
     this.router.navigate(['/blank-page']);
   }
  }

  irCategoria(id, nombre) {
    this.nombre = 'algo';
    this.descripcion = 'algo';
    localStorage.setItem('id_categoria', id);
    localStorage.setItem('nombre_cat', nombre);
    this.router.navigate(['/productos']);
  }
  verificarCrear (respuesta) {
    if (respuesta === 'Error') {
      alert('No se puede crear categoria, intente de nuevo');
    } else {
      this.router.navigate(['/blank-page']);
    }
  }

  verificarEditar (respuesta) {
    if (respuesta === 'Error') {
      alert('No se puede editar categoria, intente de nuevo');
    } else {
      this.router.navigate(['/blank-page']);
    }
  }

}
