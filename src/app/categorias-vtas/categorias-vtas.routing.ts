import { Routes } from '@angular/router';

import { CategoriasVtasComponent } from './categorias-vtas.component';

export const CategoriasVtasRoutes: Routes = [
  {
    path: '',
    component: CategoriasVtasComponent,
    data: {
      heading: 'Categorias'
    }
  }
];
