import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {
  NgbProgressbarModule,
  NgbTabsetModule
} from '@ng-bootstrap/ng-bootstrap';

import { CategoriasVtasComponent } from './categorias-vtas.component';
import { CategoriasVtasRoutes } from './categorias-vtas.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CategoriasVtasRoutes),
    NgbProgressbarModule,
    NgbTabsetModule,
    FormsModule
  ],
  declarations: [CategoriasVtasComponent]
})
export class CategoriasVtasModule {}
