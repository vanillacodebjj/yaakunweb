import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { DragulaModule } from 'ng2-dragula';

import { TiposGastosComponent } from './tipos-gastos.component';
import { TiposGastosRoutes } from './tipos-gastos.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TiposGastosRoutes),
    DragulaModule.forRoot(),
    FormsModule
  ],
  declarations: [TiposGastosComponent]
})
export class TiposGastosModule {}
