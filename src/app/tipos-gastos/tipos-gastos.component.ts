import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {HttpClient} from '@angular/common/http';
@Component({
  selector: 'app-tipos-gastos',
  templateUrl: './tipos-gastos.component.html',
  styleUrls: ['./tipos-gastos.component.scss']
})
export class TiposGastosComponent implements OnInit {
  admin: any;
  usuario: any;
  repartidor: any;
  rol: any;
  agregar: any;
  descripcion: any;
  descripcion_a: any;
  editarCat: any;
  lista: any;
  id_tipo_gasto: any;
  id_cat: any;
  mensaje: any;
  nombre: any;
  nombre_a: any;
  respuesta: any;
  tipo_a: any;
  tipo_e: any;
  user: any;
  tiposGastos: any [];
  resultado: any [];
  constructor(private http: HttpClient, public router: Router) {
    this.lista = true;
    this.editarCat = false;
    this.agregar = false;
    this.getTipoGastos();
    this.rol = localStorage.getItem('rol');
    switch (this.rol) {
      case 'Admin':
        this.admin = true;
        this.usuario = true;
        this.repartidor = true;
        break;
      case 'Usuario':
        this.admin = false;
        this.usuario = true;
        this.repartidor = false;
        break;
      case 'Repartidor':
        this.admin = false;
        this.usuario = false;
        this.repartidor = true;
        break;
    }
  }

  ngOnInit() {
  }

  agregarTipoGasto() {
    this.nombre = '';
    this.descripcion = '';
    this.agregar = true;
    this.lista = false;
    this.editarCat = false;
  }

  back() {
    this.lista = true;
    this.editarCat = false;
    this.agregar = false;
  }

  editarTipoGasto(id) {
    this.id_cat = id;
    this.editarCat = true;
    this.lista = false;
    this.agregar = false;
    this.getInfoTipoGasto();
  }

  eliminarTipoGasto(id) {
    this.http.post('http://localhost:8080/srv-3s/crudTiposGastos.php', {
        id: id,
        opcion: 'eliminarTipoGasto'
      }
    ).subscribe(
      (data: any) => {
        this.resultado = data;
        this.respuesta = data.respuesta;
        console.log('res: ' + this.respuesta);
        this.router.navigate(['/blank-page']);
      }
    );
  }

  eliminarConfirmar(id) {
    this.mensaje = confirm('Al aceptar se borrará el tipo y su herencia');
    if (this.mensaje) {
      this.eliminarTipoGasto(id);
    } else {
      alert('¡No se elimino!');
    }
  }

  eliminarPreTipoGasto(id, nombre) {
    this.mensaje = confirm('¿Desea remover ' + nombre + ' ?');
    if (this.mensaje) {
      this.eliminarConfirmar(id);
    } else {
      // alert('¡No se elimino!');
    }
  }

  getTipoGastos() {
    this.http.get('http://localhost:8080/srv-3s/getTipoGastos.php').subscribe(
      (data: any) => {
        // console.log(data);
        this.tiposGastos = data;
        // this.id = this.categorias[0];
      }
    );
  }
  getInfoTipoGasto() {
    this.http.post('http://localhost:8080/srv-3s/crudTiposGastos.php', {
        id: this.id_cat,
        opcion: 'getInfoTipoGasto'
      }
    ).subscribe(
      (data: any) => {
        this.resultado = data;
        this.nombre = this.resultado[0].nombre;
        this.descripcion = this.resultado[0].descripcion;
        this.tipo_e = this.resultado[0].tipo;
        ((document.getElementById('tipo_e') as HTMLInputElement).value) = this.tipo_e;
        // this.respuesta = this.resultado[0].respuesta;
      }
    );
  }

  guardarTipoGasto() {
    /*this.nombre = ((document.getElementById('nombre_a') as HTMLInputElement).value);
    this.descripcion = ((document.getElementById('descripcion_a') as HTMLInputElement).value);*/
    if (this.nombre_a === '' || !this.nombre_a) {
      alert('El campo nombre no puede ir vacio');
    } else if (!this.tipo_a) {
      alert('El campo tipo debe ser llenado');
    } else {
      this.http.post('http://localhost:8080/srv-3s/crudTiposGastos.php', {
          id: this.id_cat,
          nombre: this.nombre_a,
          descripcion: this.descripcion_a,
          tipo: this.tipo_a,
          opcion: 'registrarTipoGasto'
        }
      ).subscribe(
        (data: any) => {
          this.resultado = data;
          this.respuesta = data.respuesta;
          console.log('res: ' + this.respuesta);
          this.verificarCrear(this.respuesta);
          this.editarCat = false;
          this.lista = true;
          // this.respuesta = this.resultado[0].respuesta;
        }
      );
      this.router.navigate(['/blank-page']);
    }
  }

  guardarTipoGastoEditar() {
    this.http.post('http://localhost:8080/srv-3s/crudTiposGastos.php', {
        id: this.id_cat,
        nombre: this.nombre,
        descripcion: this.descripcion,
        tipo: this.tipo_e,
        opcion: 'editarTipoGasto'
      }
    ).subscribe(
      (data: any) => {
        this.resultado = data;
        this.respuesta = data.respuesta;
        console.log('res: ' + this.respuesta);
        this.verificarEditar(this.respuesta);
        // this.respuesta = this.resultado[0].respuesta;
      }
    );
    this.router.navigate(['/blank-page']);
  }

  irTipoGasto(id, nombre) {
    this.nombre = 'algo';
    this.descripcion = 'algo';
    localStorage.setItem('id_categoria', id);
    localStorage.setItem('nombre_cat', nombre);
    this.router.navigate(['/productos']);
  }
  verificarCrear (respuesta) {
    if (respuesta === 'Error') {
      alert('No se puede crear categoria, intente de nuevo');
    } else {
      this.router.navigate(['/blank-page']);
    }
  }

  verificarEditar (respuesta) {
    if (respuesta === 'Error') {
      alert('No se puede editar categoria, intente de nuevo');
    } else {
      this.router.navigate(['/blank-page']);
    }
  }

}
