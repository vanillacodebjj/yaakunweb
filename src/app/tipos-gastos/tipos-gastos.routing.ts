import { Routes } from '@angular/router';

import { TiposGastosComponent } from './tipos-gastos.component';

export const TiposGastosRoutes: Routes = [
  {
    path: '',
    component: TiposGastosComponent,
    data: {
      heading: 'Tipos Gastos',
      removeFooter: true
    }
  }
];
