import { NgModule } from '@angular/core';
import {Router, RouterModule} from '@angular/router';
import { CommonModule } from '@angular/common';

import { DragulaModule } from 'ng2-dragula';
import { DataTablesModule } from 'angular-datatables';

import { TaskboardComponent } from './taskboard.component';
import { TaskboardRoutes } from './taskboard.routing';
import {HttpClient} from '@angular/common/http';


@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(TaskboardRoutes),
    DragulaModule.forRoot()
  ],
  declarations: [TaskboardComponent]
})
export class TaskboardModule {

}
