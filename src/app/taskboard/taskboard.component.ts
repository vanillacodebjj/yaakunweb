import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';
import swal from 'sweetalert2';
import {Subject} from 'rxjs';
import { Router } from '@angular/router';



interface Tasks {
  title: string;
  description: string;
  class?: string;
}

interface Taskboard {
  title: string;
  tasks: Tasks[];
}

@Component({
  selector: 'app-taskboard',
  templateUrl: './taskboard.component.html',
  styleUrls: ['./taskboard.component.scss']
})
export class TaskboardComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject <any> = new Subject();
  id_usuario: any;
  conv: any = [];
  sesion: any;


  constructor( public http: HttpClient, public router: Router) {

    this.id_usuario = localStorage.getItem('id_usuario');
    this.convocatorias();
  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json'
      },
    };
  }

  convocatorias() {
    this.http.post('http://www.yoloiztac.com.mx/yolo/yaakun/getConvocatoriaU.php', {id: this.id_usuario}).subscribe((data: any) => {
      console.log(data);
      if (data === null) {
        swal.fire({  type: 'warning',
          title: 'Sin servicios',
          text: 'Aun no cuentas con servicios registrados',
          confirmButtonColor: '#400342',
          customClass: {
            popup: 'animated tada'
          }
        });
        this.dtTrigger.next();
      } else {
        this.conv = data;
        this.dtTrigger.next();
      }
    });
  }
}
