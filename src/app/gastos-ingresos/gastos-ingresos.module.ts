import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { DragulaModule } from 'ng2-dragula';

import { GastosIngresosComponent } from './gastos-ingresos.component';
import { GastosIngresosRoutes } from './gastos-ingresos.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(GastosIngresosRoutes),
    DragulaModule.forRoot(),
    FormsModule
  ],
  declarations: [GastosIngresosComponent]
})
export class GastosIngresosModule {}
