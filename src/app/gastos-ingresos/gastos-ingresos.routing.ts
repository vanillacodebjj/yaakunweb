import { Routes } from '@angular/router';

import { GastosIngresosComponent } from './gastos-ingresos.component';

export const GastosIngresosRoutes: Routes = [
  {
    path: '',
    component: GastosIngresosComponent,
    data: {
      heading: 'Gastos',
      removeFooter: true
    }
  }
];
