import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-gastos-ingresos',
  templateUrl: './gastos-ingresos.component.html',
  styleUrls: ['./gastos-ingresos.component.scss']
})
export class GastosIngresosComponent implements OnInit {
  admin: any;
  usuario: any;
  agregar: any;
  concepto: any;
  descripcion: any;
  editar: any;
  fecha_realizada: any;
  lista: any;
  monto: any;
  tipo_gasto_id;
  rol: any;
  gastos_ingresos: any[];
  constructor(private http: HttpClient, public router: Router) {
    this.rol = localStorage.getItem('rol');
    switch (this.rol) {
      case 'Admin':
        this.admin = true;
        this.usuario = true;
        break;
      case 'Usuario':
        this.admin = false;
        this.usuario = true;
        break;
    }
    this.getGatng();
  }

  ngOnInit() {
  }

  agregarGasIng() {}

  back() {}

  getGatng() {
    this.http.get('http://localhost:8080/srv-3s/getGasIng.php').subscribe(
      (data: any) => {
        // console.log(data);
        this.gastos_ingresos = data;
        // this.id = this.categorias[0];
      }
    );
  }

}
