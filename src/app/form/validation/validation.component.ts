import { Component, OnInit } from '@angular/core';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import swal from'sweetalert2';

import { CustomValidators } from 'ng2-validation';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.scss']
})
export class ValidationComponent implements OnInit {
  form: FormGroup;
  num = 5;
  fechaC: any;
  fechaIS: any;
  fechaCS: any;
  personas: any;
  estado: any;
  municipio: any;
  closeResult: string;
  nombre: any;
  descripcion: any;
  descripcionC: any;
  direccion: any;
  costo: any;
  presupuesto: any;

  constructor(public  modalService: NgbModal, private router: Router) {

  }

  ngOnInit() {
    const password = new FormControl('', Validators.required);
    const certainPassword = new FormControl(
      '',
      CustomValidators.notEqualTo(password)
    );

    this.form = new FormGroup({
      password: password,
      certainPassword: certainPassword
    });

    // this.fecha = localStorage.getItem('fecha');
    this.personas = 1;
    this.costo = 0;
    this.presupuesto = 0;
  }

  onSubmit(form) {
    console.log(form);
  }


  registrar(){
    swal.fire({
      type: 'success',
      title: 'Se registro correctamente',
      showConfirmButton: false,
      timer: 1500
    });
    this.router.navigate(['/calendar']);
  }


  /*reservacionR() {
    alert('Tu reservación a sido registrada');
    this.router.navigate(['/calendar']);
  }*/

  /*open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }*/
}
