import { Routes } from '@angular/router';

import { BlankPageComponent } from './blank-page.component';

export const BlankPageRoutes: Routes = [
  {
    path: '',
    component: BlankPageComponent,
    data: {
      heading: 'Blank'
    }
  }
];
