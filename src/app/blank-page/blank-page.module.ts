import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {
  NgbProgressbarModule,
  NgbTabsetModule
} from '@ng-bootstrap/ng-bootstrap';

import { BlankPageComponent } from './blank-page.component';
import { BlankPageRoutes } from './blank-page.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(BlankPageRoutes),
    NgbProgressbarModule,
    NgbTabsetModule
  ],
  declarations: [BlankPageComponent]
})
export class BlankPageModule {}
