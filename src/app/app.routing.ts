  import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './core';
import { AuthLayoutComponent } from './core';

export const AppRoutes: Routes = [
  {
    path: '',
    component: AdminLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './taskboard/taskboard.module#TaskboardModule'
      },
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'abonos-gastos',
        loadChildren: './abonos-gastos/abonos-gastos.module#AbonosGastosModule'
      },
      {
        path: 'blank-page',
        loadChildren: './blank-page/blank-page.module#BlankPageModule'
      },
      {
        path: 'email',
        loadChildren: './email/email.module#EmailModule'
      },
      {
        path: 'categorias-vtas',
        loadChildren: './categorias-vtas/categorias-vtas.module#CategoriasVtasModule'
      },
      {
        path: 'components',
        loadChildren: './components/components.module#ComponentsModule'
      },
      {
        path: 'cards',
        loadChildren: './cards/cards.module#CardsModule'
      },
      {
        path: 'gastos-ingresos',
        loadChildren: './gastos-ingresos/gastos-ingresos.module#GastosIngresosModule'
      },
      {
        path: 'forms',
        loadChildren: './form/form.module#FormModule'
      },
      {
        path: 'tables',
        loadChildren: './tables/tables.module#TablesModule'
      },
      {
        path: 'tipos-gastos',
        loadChildren: './tipos-gastos/tipos-gastos.module#TiposGastosModule'
      },
      {
        path: 'datatable',
        loadChildren: './datatable/datatable.module#DatatableModule'
      },
      {
        path: 'charts',
        loadChildren: './charts/charts.module#ChartsModule'
      },
      {
        path: 'instituciones',
        loadChildren: './instituciones/instituciones.module#InstitucionesModule'
      },
      {
        path: 'maps',
        loadChildren: './maps/maps.module#MapsModule'
      },
      {
        path: 'pages',
        loadChildren: './pages/pages.module#PagesModule'
      },
      {
        path: 'taskboard',
        loadChildren: './taskboard/taskboard.module#TaskboardModule'
      },
      {
        path: 'calendar',
        loadChildren: './fullcalendar/fullcalendar.module#FullcalendarModule'
      },
      {
        path: 'media',
        loadChildren: './media/media.module#MediaModule'
      },
      {
        path: 'skills',
        loadChildren: './skills/skills.module#SkillsModule'
      },
      {
        path: 'docs',
        loadChildren: './docs/docs.module#DocsModule'
      }
    ]
  },
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {
        path: 'account',
        loadChildren: './account/account.module#AccountModule'
      },
      {
        path: 'error',
        loadChildren: './error/error.module#ErrorModule'
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'error/404'
  }
];
