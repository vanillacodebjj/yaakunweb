import {
  Component,
  ChangeDetectionStrategy,
  Inject,
  TemplateRef,
  ViewChild,
  OnInit
} from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import swal from'sweetalert2';
import { Subject } from 'rxjs';

import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';

import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent
} from 'angular-calendar';
import {Router} from '@angular/router';

import {HttpClient} from '@angular/common/http';

/*const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};*/

@Component({
  selector: 'app-fullcalendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './fullcalendar.component.html',
  styleUrls: ['./fullcalendar.component.scss']
})
export class FullcalendarComponent implements OnInit {
  @ViewChild('modalContent')
  // @ViewChild('modalContent2')
 // modalContent: TemplateRef<any>;
  // modalContent2: TemplateRef<any>;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject <any> = new Subject();
  id_usuario: any;
  conv: any = [];

  // persons: any = [];

  // view = 'month';

  /*viewDate: Date = new Date();
  mes: any;
  fecha: any;*/

  modalData: {
    action: string;
    event: CalendarEvent;
  };



/* actions: CalendarEventAction[] = [
    {
      label: '<i class="editButton"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        // this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="deleteButton"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        // this.handleEvent('Deleted', event);
      }
    }
  ];*/

 /* refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [
    {
      start: subDays(startOfDay(new Date()), 1),
      end: addDays(new Date(), 1),
      title: 'A 3 day event',
      color: colors.red,
      // actions: this.actions
    },
    {
      start: startOfDay(new Date()),
      title: 'An event with no end date',
      color: colors.yellow,
      // actions: this.actions
    },
    {
      start: subDays(endOfMonth(new Date()), 3),
      end: addDays(endOfMonth(new Date()), 3),
      title: 'A long event that spans 2 months',
      color: colors.blue
    },
    {
      start: addHours(startOfDay(new Date()), 2),
      end: new Date(),
      title: 'A draggable and resizable event',
      color: colors.yellow,
      // actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    }
  ];*/

  // activeDayIsOpen = true;

  constructor(private modal: NgbModal, private router: Router, public http: HttpClient) {
    this.id_usuario = localStorage.getItem('id_usuario');
    this.convocatorias();
  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json'
      },
    };
  }

  convocatorias() {
    this.http.post('http://www.yoloiztac.com.mx/yolo/yaakun/getConvocatoriaU.php', {id: this.id_usuario}).subscribe((data: any) => {
      console.log(data);
      if (data === null) {
        swal.fire({  type: 'warning',
          title: 'Sin convocatorias',
          text: 'Aun no cuentas con convocatorias publicadas',
          confirmButtonColor: '#400342',
          customClass: {
            popup: 'animated tada'
          }
        });
        this.dtTrigger.next();
      } else {
        this.conv = data;
        this.dtTrigger.next();
      }
    });
  }

 /* dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.mes = date.getMonth() + 1;
        this.activeDayIsOpen = false;
        console.log('Sin evento Mes : ' + this.mes);
        console.log('Sin evento Año: ' + date.getFullYear());
        console.log('Sin evento Dia: ' + date.getDate());
       if (this.mes >= 10 && date.getDate() >= 10  ) {
         this.fecha = date.getFullYear() + '-' + this.mes + '-' + date.getDate();
       } else {
         if (this.mes >= 10 ) {
           this.fecha = date.getFullYear() + '-' + this.mes + '-0' + date.getDate();
         } else {
           if (date.getDate() >= 10 ) {
             this.fecha = date.getFullYear() + '-0' + this.mes + '-' + date.getDate();
           } else {
             this.fecha = date.getFullYear() + '-0' + this.mes + '-0' + date.getDate();
           }
         }
       }
       console.log(this.fecha);

        localStorage.setItem('fecha', this.fecha);
        this.router.navigate(['/forms/validation']);
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
        console.log('Con evento');
      }
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  addEvent(): void {
    this.events.push({
      title: 'New event',
      start: startOfDay(new Date()),
      end: endOfDay(new Date()),
      color: colors.red,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      }
    });
    this.refresh.next();
  }*/
}
