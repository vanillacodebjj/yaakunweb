import { Component, EventEmitter, Output, Input } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  @Input()
  heading: string;
  @Output()
  toggleSidenav = new EventEmitter<void>();
  nombre: any;

  constructor() {
    this.nombre = localStorage.getItem('username');
  }
}
