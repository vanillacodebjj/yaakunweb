import { Routes } from '@angular/router';

import { AbonosGastosComponent } from './abonos-gastos.component';

export const AbonosGastosRoutes: Routes = [
  {
    path: '',
    component: AbonosGastosComponent,
    data: {
      heading: 'Abonos a gastos',
      removeFooter: true
    }
  }
];
