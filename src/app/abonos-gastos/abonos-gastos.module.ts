import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DragulaModule } from 'ng2-dragula';

import { AbonosGastosComponent } from './abonos-gastos.component';
import { AbonosGastosRoutes } from './abonos-gastos.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AbonosGastosRoutes),
    DragulaModule.forRoot()
  ],
  declarations: [AbonosGastosComponent]
})
export class AbonosGastosModule {}
