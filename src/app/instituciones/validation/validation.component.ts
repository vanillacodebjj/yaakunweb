import { Component, OnInit } from '@angular/core';
import { Validators, FormControl, FormGroup } from '@angular/forms';

import { CustomValidators } from 'ng2-validation';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.scss']
})
export class ValidationComponent implements OnInit {
  form: FormGroup;
  num = 5;
  fecha: any;
  personas: any;
  hora: any;
  mail: any;
  forma_p: any;
  forma_pR: any;
  referencia: any;
  telefono: any;
  closeResult: string;
  nombre: any;
  a_paterno: any;
  a_materno: any;
  total_personas = [];
  nombreC: any;
  a_paternoC: any;
  a_maternoC: any;
  edadC: any;
  estaturaC: any;
  pesoC: any;
  nivelC: any;
  montadoC: any;
  aux: any;

  constructor(public  modalService: NgbModal, private router: Router) {

  }

  ngOnInit() {
    const password = new FormControl('', Validators.required);
    const certainPassword = new FormControl(
      '',
      CustomValidators.notEqualTo(password)
    );

    this.form = new FormGroup({
      password: password,
      certainPassword: certainPassword
    });

    this.fecha = localStorage.getItem('fecha');
    this.personas = 1;
  }

  onSubmit(form) {
    console.log(form);
  }

  ModalPersonas( modal) {
    if (this.nombre === '' || this.a_materno === '' || this.a_paterno === '' || this.hora === '' || this.telefono === '' || this.mail === '' || this.forma_p === '' || this.referencia === '' || this.forma_pR === '' || this.nombre === undefined || this.a_materno === undefined || this.a_paterno === undefined || this.hora === undefined || this.telefono === undefined || this.mail === undefined || this.forma_p === undefined || this.referencia === undefined || this.forma_pR === undefined || this.nombre === null || this.a_materno === null || this.a_paterno === null || this.hora === null || this.telefono === null || this.mail === null || this.forma_p === null || this.referencia === null || this.forma_pR === null ) {
      alert('Favor de llenar todos los campos');
    } else {
      this.open(modal);
      this.aux = this.personas;
    }
  }

  registrar(modal) {
    console.log(this.aux);
    if (this.nombreC === '' || this.a_paternoC === '' || this.a_maternoC === '' || this.edadC === '' || this.estaturaC === '' || this.pesoC === '' || this.nivelC === '' || this.montadoC === '' || this.nombreC === undefined || this.a_paternoC === undefined || this.a_maternoC === undefined || this.edadC === undefined || this.estaturaC === undefined || this.pesoC === undefined || this.nivelC === undefined || this.montadoC === undefined || this.nombreC === null || this.a_paternoC === null || this.a_maternoC === null || this.edadC === null || this.estaturaC === null || this.pesoC === null || this.nivelC === null || this.montadoC === null) {
      alert('Favor de llenar todos los campos');
    } else {
      this.total_personas.push({
        'nombre': this.nombreC,
        'a_paterno': this.a_paternoC,
        'a_materno': this.a_maternoC,
        'edad': this.edadC,
        'estatura': this.estaturaC,
        'peso': this.pesoC,
        'nivel_experiencia': this.nivelC,
        'veces_montado': this.montadoC
      });
      console.log(this.total_personas);
      this.aux--;
      this.nombreC = '';
      this.a_paternoC = '';
      this.a_maternoC = '';
      this.edadC = '';
      this.estaturaC = '';
      this.pesoC = '';
      this.nivelC = '';
      this.montadoC = '';
      alert('Clinete registrado');
      if (this.aux === 0) {
        this.modalService.dismissAll();
        this.modalService.open(modal);
      }
    }
  }

  reservacionR() {
    alert('Tu reservación a sido registrada');
    this.router.navigate(['/calendar']);
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
