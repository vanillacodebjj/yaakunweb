import { Routes } from '@angular/router';

import { SkillsComponent } from './skills.component';

export const SocialRoutes: Routes = [
  {
    path: '',
    component: SkillsComponent,
    data: {
      heading: 'Categorias'
    }
  }
];
