import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent {

  datos: any = [];

  constructor(public http: HttpClient, public router: Router) {
    this.getCategorias();
  }
  getCategorias() {
    this.http.get('http://www.yoloiztac.com.mx/yolo/yaakun/getCategorias.php').subscribe((data: any) => {
      console.log(data);
      this.datos = data;
    });
  }

  ver(id) {
    localStorage.setItem('id_categoria', id);
    this.router.navigate(['/cards/draggable']);
  }
}
