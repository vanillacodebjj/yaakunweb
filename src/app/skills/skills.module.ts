import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {
  NgbProgressbarModule,
  NgbTabsetModule
} from '@ng-bootstrap/ng-bootstrap';

import { SkillsComponent } from './skills.component';
import { SocialRoutes } from './skills.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SocialRoutes),
    NgbProgressbarModule,
    NgbTabsetModule
  ],
  declarations: [SkillsComponent]
})
export class SkillsModule {}
